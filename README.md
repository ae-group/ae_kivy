<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# kivy 0.3.120

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy/release0.3.119?logo=python)](
    https://gitlab.com/ae-group/ae_kivy/-/tree/release0.3.119)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy)](
    https://pypi.org/project/ae-kivy/#history)

>ae_kivy package 0.3.120.

[![Coverage](https://ae-group.gitlab.io/ae_kivy/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy)](
    https://gitlab.com/ae-group/ae_kivy/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy)](
    https://gitlab.com/ae-group/ae_kivy/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy)](
    https://gitlab.com/ae-group/ae_kivy/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy)](
    https://pypi.org/project/ae-kivy/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy)](
    https://gitlab.com/ae-group/ae_kivy/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy)](
    https://libraries.io/pypi/ae-kivy)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy)](
    https://pypi.org/project/ae-kivy/#files)


## installation


execute the following command to install the
ae.kivy package
in the currently active virtual environment:
 
```shell script
pip install ae-kivy
```

if you want to contribute to this portion then first fork
[the ae_kivy repository at GitLab](
https://gitlab.com/ae-group/ae_kivy "ae.kivy code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy.html
"ae_kivy documentation").
